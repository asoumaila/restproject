package org.projet.samples.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.projet.samples.beans.Competence;
import org.projet.samples.beans.Competences;
import org.projet.samples.beans.TypeProjet;
import org.projet.samples.beans.TypeProjets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class Dao implements IDao {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private SimpleJdbcTemplate simpleJdbcTemplate;

	@Autowired
	public void init(DataSource dataSource) {
		this.simpleJdbcTemplate = new SimpleJdbcTemplate(dataSource);
		// this.procReadTp = new SimpleJdbcCall(dataSource)
		// .withProcedureName("typrojet");

	}

	@Override
	public TypeProjet getOne(int id) {
		// TODO Auto-generated method stub
		TypeProjet projet = simpleJdbcTemplate
				.queryForObject(
						"select id_projet,nom_projet from  projet where id_projet=:id_tp",
						BeanPropertyRowMapper.newInstance(TypeProjet.class),
						new MapSqlParameterSource().addValue("id_tp", id));
		if (projet != null) {
			return projet;
		} else
			throw new DataRetrievalFailureException("null");

	}

	@Override
	public TypeProjet getOneByName(String name) {
		// TODO Auto-generated method stub
		TypeProjet projet = simpleJdbcTemplate
				.queryForObject(
						"select id_projet,nom_projet from  projet where nom_projet=:name",
						BeanPropertyRowMapper.newInstance(TypeProjet.class),
						new MapSqlParameterSource().addValue("name", name));
		if (projet != null) {
			return projet;
		} else
			throw new DataRetrievalFailureException("null");

	}

	@Override
	public void addOneProjet(TypeProjet projet) {
		// TODO Auto-generated method stub
		String query = "insert into projet(nom_projet) values (:nom_projet)";
		SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(
				projet);
		simpleJdbcTemplate.update(query, fileParameters);

	}

	@Override
	public Competence getOneById(int id) {
		// TODO Auto-generated method stub
		Competence comp = simpleJdbcTemplate
				.queryForObject(
						"select id_comp,nom_comp from competence where id_comp=:id_comp",
						BeanPropertyRowMapper.newInstance(Competence.class),
						new MapSqlParameterSource().addValue("id_comp", id));
		if (comp != null) {
			return comp;
		} else
			throw new DataRetrievalFailureException("null");

	}

	@Override
	public void addOneComp(Competence comp) {
		// TODO Auto-generated method stub
		String query = "insert into competence(nom_comp) values (:nom_comp)";
		SqlParameterSource fileParameters = new BeanPropertySqlParameterSource(
				comp);
		simpleJdbcTemplate.update(query, fileParameters);

	}

	@Override
	public Competence getOneComp(String name) throws DataAccessException {
		// TODO Auto-generated method stub
		Competence comp = simpleJdbcTemplate
				.queryForObject(
						"select id_comp,nom_comp from  competence where nom_comp=:name",
						BeanPropertyRowMapper.newInstance(Competence.class),
						new MapSqlParameterSource().addValue("name", name));
		if (comp != null) {
			return comp;
		} else
			throw new DataRetrievalFailureException("null");
	}

	@Override
	public TypeProjets getProjets() {
		// TODO Auto-generated method stub
		TypeProjets lesProjets = new TypeProjets();
		List<TypeProjet> list = simpleJdbcTemplate.query(
				"select id_projet,nom_projet from projet",
				BeanPropertyRowMapper.newInstance(TypeProjet.class));

		lesProjets.setProjets(list);
		return lesProjets;
	}

	@Override
	public Competences getComps() {
		// TODO Auto-generated method stub
		Competences comp = new Competences();
		List<Competence> list = simpleJdbcTemplate.query(
				"select id_comp,nom_comp from competence",
				BeanPropertyRowMapper.newInstance(Competence.class));
		comp.setCompetences(list);
		return comp;
	}

	@Override
	public List<Competence> getCompofty(int id_projet) {
		// TODO Auto-generated method stub
		List<Competence> list = simpleJdbcTemplate
				.query("select c.id_comp,c.nom_comp from competence c,service s where s.id_projet=:id_projet and s.id_comp=c.id_comp",
						BeanPropertyRowMapper.newInstance(Competence.class),
						new MapSqlParameterSource().addValue("id_projet",
								id_projet));
		return list;

	}

	@Override
	public List<Competence> getCompNotType(int id_projet) {
		// TODO Auto-generated method stub
		List<Competence> list = simpleJdbcTemplate
				.query("select id_comp,nom_comp from competence where id_comp NOT IN (select c.id_comp from competence c,service s where s.id_projet=:id_projet and s.id_comp=c.id_comp)",
						BeanPropertyRowMapper.newInstance(Competence.class),
						new MapSqlParameterSource().addValue("id_projet",
								id_projet));
		return list;
	}

	@Override
	public void linkProjetComp(int id_projet, int id_comp) {
		// TODO Auto-generated method stub
		Map<String, Integer> parameters = new HashMap<String, Integer>();
		parameters.put("id_pr", id_projet);
		parameters.put("id_com", id_comp);
		String req = "insert into service(id_projet,id_comp) values ("
				+ id_projet + "," + id_comp + ")";
		simpleJdbcTemplate.update(req);

	}
}
