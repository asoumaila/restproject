package org.projet.samples.beans;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "COMPETENCES")
public class Competences {

	private List<Competence> competences;

	@XmlElementWrapper(name = "competences")
	@XmlElement(name = "comPetence")
	public List<Competence> getCompetences() {
		return competences;
	}

	public void setCompetences(List<Competence> competences) {
		this.competences = new ArrayList<Competence>(competences);
	}

}
