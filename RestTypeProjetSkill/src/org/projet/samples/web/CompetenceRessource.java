package org.projet.samples.web;

import java.net.URI;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;

import org.projet.samples.beans.Competence;
import org.projet.samples.beans.Competences;
import org.projet.samples.service.IService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Path("/competences")
public class CompetenceRessource {

	private IService service;

	public IService getService() {
		String chemin = "applicationContext-jdbc.xml";
		ApplicationContext context = new ClassPathXmlApplicationContext(chemin);
		return service = (IService) context.getBean("service");
	}

	// @GET
	// @Path("{id}")
	// @Produces({ javax.ws.rs.core.MediaType.APPLICATION_XML })
	// public Competence getCompById(@PathParam("id") int id) {
	// service = getService();
	// return service.getComp(id);
	//
	// }

	@GET
	@Path("{name}")
	@Produces({ javax.ws.rs.core.MediaType.APPLICATION_XML })
	public Competence getCompByName(@PathParam("name") String name) {
		service = getService();
		return service.getCompByName(name);

	}

	@GET
	@Path("")
	@Produces({ javax.ws.rs.core.MediaType.APPLICATION_XML })
	public Competences getComps() {
		service = getService();
		return service.getAllComp();

	}

	@POST
	@Consumes({ MediaType.APPLICATION_XML })
	@Path("newskill")
	public Response ajoutComp(JAXBElement<Competence> comp, @Context UriInfo uri) {
		Competence competence = comp.getValue();

		service = getService();

		service.addComp(competence);

		URI absolutePath = uri.getAbsolutePathBuilder()
				.path(competence.getNom_comp()).build();
		return Response.created(absolutePath).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Path("listCompetences")
	public void addListComp(JAXBElement<Competences> Comps) {
		Competences comp = Comps.getValue();
		service = getService();
		for (Competence p : comp.getCompetences()) {

			service.addComp(p);
		}

	}

}
