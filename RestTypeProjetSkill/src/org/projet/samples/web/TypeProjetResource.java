package org.projet.samples.web;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;

import org.projet.samples.beans.Competence;
import org.projet.samples.beans.TypeProjet;
import org.projet.samples.beans.TypeProjets;
import org.projet.samples.service.IService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Path("projets")
public class TypeProjetResource {
	private IService service;

	public IService getService() {
		String chemin = "applicationContext-jdbc.xml";
		ApplicationContext context = new ClassPathXmlApplicationContext(chemin);
		return service = (IService) context.getBean("service");
	}

	// @GET
	// @Path("{id}")
	// @Produces({ javax.ws.rs.core.MediaType.APPLICATION_XML })
	// public TypeProjet getTypeProjet(@PathParam("id") int id) {
	// service = getService();
	// return service.getTyprojet(id);
	//
	// }

	@GET
	@Path("{name}")
	@Produces({ javax.ws.rs.core.MediaType.APPLICATION_XML })
	public TypeProjet getTprojetByName(@PathParam("name") String name) {
		service = getService();
		return service.getProjetByName(name);

	}

	@GET
	@Path("")
	@Produces({ javax.ws.rs.core.MediaType.APPLICATION_XML })
	public TypeProjets getProjets() {
		service = getService();
		return service.getAllProjet();

	}

	@POST
	@Consumes({ MediaType.APPLICATION_XML })
	@Path("newprojet")
	public Response ajoutProjet(JAXBElement<TypeProjet> projet,
			@Context UriInfo uri) {
		TypeProjet pro = projet.getValue();

		service = getService();

		service.addprojet(pro);

		URI absolutePath = uri.getAbsolutePathBuilder()
				.path(pro.getNom_projet()).build();
		return Response.created(absolutePath).build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_XML })
	@Path("{name}/competences")
	public List<Competence> listCompforproject(@PathParam("name") String name) {

		service = getService();
		TypeProjet projet = service.getProjetByName(name);
		return service.getCompofprojet(projet.getId_projet());

	}

	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Path("listProjets")
	public void getListProjets(JAXBElement<TypeProjets> ListProjets) {
		TypeProjets projets = ListProjets.getValue();
		service = getService();
		for (TypeProjet p : projets.getProjets()) {

			service.addprojet(p);
		}

	}

}
